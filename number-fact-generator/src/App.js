import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import $ from 'jquery'

import GenerateButton from './components/GenerateButton'
import TextInput from './components/TextInput'
import Fact from './components/Fact'


class App extends Component {
  constructor() {
    super()

    this.state = {
      number: null,
      fact: "type a number or click button for a number fact"
    }

    this.onHandleClick = this.onHandleClick.bind(this)
    this.onHandleTyping = this.onHandleTyping.bind(this)
  }

  onHandleClick() {
    const n = Math.floor(Math.random() * 99)
    this.fetchNumberFactApi(n)
  }


  onHandleTyping = (event) => {
    this.fetchNumberFactApi(event.target.value)
  }

  fetchNumberFactApi(number) {
    if (number == null) {
      return
    }

    const url = "http://numbersapi.com/" + number + "?json"

    $.getJSON(url)
      .then((result) => this.setState({
        number: result.number,
        fact: result.text
      }))

  }


  render() {
    return (
      <div className="App">
        
        <Fact number={this.state.number} fact={this.state.fact}/>

        <hr />

        <TextInput label="Type a number" onChange={this.onHandleTyping}></TextInput>
        
        <h3>or</h3>

        <GenerateButton onClick={this.onHandleClick}>Generate Random Number Fact</GenerateButton>

      </div>
    );
  }
}

export default App;
