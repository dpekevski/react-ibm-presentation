import React from 'react'

export default function TextInput(props) {

	const style = {
		width: 40,
		marginLeft: 'auto',
		marginRight: 'auto'
	}

	return (
		<div className="bx--form-item">
			<label className="bx--label" >{props.label}</label>
			<input 
				className="bx--text-input" 
				type="input" 
				onChange={props.onChange}
				style={style} />
		</div>
	)
}