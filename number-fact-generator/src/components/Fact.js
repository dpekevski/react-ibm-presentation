import React from 'react'

export default function Fact(props) {
	const style = {
		fontSize: 40,
		padding: 50
	}

	return (
		<div style={style}>
			<h1>{props.number}</h1>
			<h2>{props.fact}</h2>
		</div>
	)
}
