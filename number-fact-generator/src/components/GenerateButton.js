import React from 'react'

export default function GenerateButton(props) {
	return (
		<button
			onClick={props.onClick}
			className="bx--btn bx--btn--primary">
			{props.children}
		</button>
	)
}